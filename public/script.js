//console.log (pierwszaLiczba + drugaLiczba);
//console.log (zmiennaTekstowa + " " + "czepliwy testerze");

//if (wiek < 18) {
//   console.log("Nie powinieneś tutaj być!")
//  }

//for (let i in nazwaZmiennej) {
// console.log("Element " + i + " to " + nazwaZmiennej[i]);

//}

let timerId;

function loadJoke() {
  clearTimeout(timerId);

  $("#joke").fadeOut(1000, () => {
    fetch("https://api.chucknorris.io/jokes/random")
      .then((response) => response.json())
      .then((response) => {
        $("#joke").html(response.value);

        $("#joke").fadeIn(1000);
      });
  });
}

loadJoke();
$("#jokeRefresh").click(() => {
  loadJoke();
  autoRefresh();
});

function autoRefresh() {
  timerId = setTimeout(() => {
    loadJoke();
    autoRefresh();
  }, 5000);
}
autoRefresh();
